#!/usr/bin/bash
# make sure we have python3-ethtool installed
if rpm -q --quiet python3-ethtool; then
    :
else
    sudo dnf install -y python3-ethtool 
    if [[ $? != 0 ]]; then
        echo "install of python3-ethtool failed!"
        exit 1
    fi
fi
 
# See if python3-ethtool's pethtool is installed,executable and help works.
pethtool --help 2>> /dev/null
if [[ $? != 0 ]]; then
    exit 2
fi
